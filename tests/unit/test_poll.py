from itertools import chain
from typing import List, Any, Tuple
from uuid import uuid4 as uuid
from pprint import pformat

from pytest import mark

from ranked_choice_bot.poll import Poll, Vote, Votes, Strings, score_in_vote
from ranked_choice_bot.poll import score, tied, RankedOptions, ScoredOptions
from ranked_choice_bot.poll import RankedOption, score_group, sort_and_tie
from ranked_choice_bot.error import InvalidOptionsRankingError
from ranked_choice_bot.log import log


def user_id() -> str:
    return str(uuid())


def flatten2d(to_flatten: List[Any]) -> List[Any]:
    return list(chain.from_iterable(to_flatten))


def test_tracker_invalid_update() -> None:
    poll = Poll('stuff', ['option1', 'option2'])
    assert isinstance(poll.vote(Vote(user_id(),
                                     ['option1',
                                      'option2',
                                      'option3'])), InvalidOptionsRankingError)
    assert isinstance(poll.vote(Vote(user_id(),
                                     ['option1',
                                      'option3'])), InvalidOptionsRankingError)


@mark.parametrize('option, options, vote, expected', [
    ('a', ['a', 'b', 'c'], ['a', 'b', 'c'], 3),
    ('b', ['a', 'b', 'c'], ['a', 'b', 'c'], 2),
    ('c', ['a', 'b', 'c'], ['a', 'b', 'c'], 1),
    ('d', ['a', 'b', 'c'], ['a', 'b', 'c'], 0),
])
def test_score_in_vote(option: str, options: Strings, vote: Vote, expected: int) -> None:
    assert score_in_vote(option, options, vote) == expected


@mark.parametrize('ranked', [
    (['a', 'b', 'c', 'd']),
    (['stuff', 'stuff1', 'stuff2']),
    ([]),
    (['a', 'b'])
])
def test_vote_serialization(ranked: Strings) -> None:
    original = Vote(user_id(), ranked)
    serialized = original.to_data()
    assert serialized == {'voter_id': original.voter_id,
                          'ranked': original.ranked}
    deserialized = Vote.from_data(serialized)
    assert original.voter_id == deserialized.voter_id
    assert original.ranked == deserialized.ranked


@mark.parametrize('option, options, votes, expected', [
    ('a', ['a', 'b', 'c'], [['a', 'b', 'c'], ['a', 'b', 'c'], ['a', 'b', 'c']], 9),
    ('b', ['a', 'b', 'c'], [['a', 'b', 'c'], ['a', 'b', 'c'], ['a', 'b', 'c']], 6),
    ('c', ['a', 'b', 'c'], [['a', 'b', 'c'], ['a', 'b', 'c'], ['a', 'b', 'c']], 3),
    ('d', ['a', 'b', 'c'], [['a', 'b', 'c'], ['a', 'b', 'c'], ['a', 'b', 'c']], 0),
    ('a', ['a', 'b', 'c'], [['b', 'c'], ['b', 'c'], ['a', 'b', 'c']], 3),
    ('b', ['a', 'b', 'c'], [['b', 'c'], ['b', 'c'], ['a', 'b', 'c']], 8),
    ('c', ['a', 'b', 'c'], [['b', 'c'], ['b', 'c'], ['a', 'b', 'c']], 5),
    ('d', ['a', 'b', 'c'], [['b', 'c'], ['b', 'c'], ['a', 'b', 'c']], 0),
    ('a', ['a', 'b', 'c'], [['b', 'c'], ['c', 'b']], 0),
    ('b', ['a', 'b', 'c'], [['b', 'c'], ['c', 'b']], 5),
    ('c', ['a', 'b', 'c'], [['b', 'c'], ['c', 'b']], 5),
    ('d', ['a', 'b', 'c'], [['b', 'c'], ['c', 'b']], 0),
])
def test_score(option: str, options: Strings, votes: Votes, expected: int) -> None:
    assert score(option, options, votes) == expected


@mark.parametrize('scored_options, expected', [
    ([('a', 1), ('b', 1), ('c', 1)], (['a', 'b', 'c'], 1)),
    ([('a', 1)], (['a'], 1)),
    ([('a', 5)], (['a'], 5)),
    ([('a', 5), ('b', 5), ('c', 5)], (['a', 'b', 'c'], 5)),
])
def test_score_group(scored_options: ScoredOptions, expected: RankedOption) -> None:
    assert score_group(scored_options) == expected


@mark.parametrize('scored_options, expected', [
    ([('a', 1), ('b', 2), ('c', 3)], [(['a'], 1), (['b'], 2), (['c'], 3)]),
    ([('a', 1), ('b', 2), ('c', 2)], [(['a'], 1), (['b', 'c'], 2)]),
    ([], []),
])
def test_tied(scored_options: ScoredOptions, expected: RankedOptions) -> None:
    assert tied(scored_options) == expected


@mark.parametrize('scored_options, expected', [
    ([('a', 1), ('b', 2), ('c', 3)], [(['c'], 3), (['b'], 2), (['a'], 1)]),
    ([('a', 1), ('b', 2), ('c', 2)], [(['b', 'c'], 2), (['a'], 1)]),
    ([], []),
])
def test_sort_and_tie(scored_options: ScoredOptions, expected: RankedOptions) -> None:
    assert sort_and_tie(scored_options) == expected


@mark.parametrize('votes, expected', [
    ([['yes'], ['yes'], ['no']], [(['yes'], 8), (['no'], 4), (['maybe', 'tomorrow'], 0)]),
    ([['yes', 'tomorrow', 'maybe', 'no'],
      ['yes', 'tomorrow', 'maybe', 'no'],
      ['yes', 'tomorrow', 'maybe', 'no'],
      ['yes', 'tomorrow', 'maybe', 'no'],
      ['no', 'maybe', 'tomorrow', 'yes'],
      ['no', 'maybe', 'tomorrow', 'yes'],
      ['no', 'maybe', 'tomorrow', 'yes']],
     [(['yes'], 19), (['tomorrow'], 18), (['maybe'], 17), (['no'], 16)]),
    ([['yes', 'tomorrow'],
      ['yes', 'tomorrow'],
      ['yes', 'tomorrow'],
      ['yes', 'tomorrow'],
      ['yes', 'tomorrow'],
      ['yes', 'tomorrow'],
      ['tomorrow', 'no'],
      ['tomorrow', 'no'],
      ['tomorrow', 'no'],
      ['tomorrow', 'maybe'],
      ['tomorrow', 'maybe'],
      ['no', 'tomorrow'],
      ['no', 'tomorrow']],
     [(['tomorrow'], 44), (['yes'], 24), (['no'], 17), (['maybe'], 6)])
])
def test_tracker(votes: List[Strings], expected: List[Strings]) -> None:
    poll = Poll('Shall we dance?', ['yes', 'no', 'maybe', 'tomorrow'])
    for vote in votes:
        poll.vote(Vote(user_id(), vote))
    assert poll.current_state == expected
    serialized = poll.to_data()
    print(pformat(serialized))
    deserialized = Poll.from_data(serialized)
    assert poll.desc == deserialized.desc
    assert poll.options == deserialized.options
    assert poll.votes == deserialized.votes


def test_create_poll() -> None:
    poll = Poll("Asking some stuff...")
    assert not poll.valid_option_idx(0)
    assert not poll.valid_option_idx(1)
    assert not poll.valid_option_one_idx(0)
    assert not poll.valid_option_one_idx(1)
    poll.add_option('stuff')
    assert poll.valid_option_idx(0)
    assert not poll.valid_option_idx(1)
    assert not poll.valid_option_one_idx(0)
    assert poll.valid_option_one_idx(1)
    poll.add_option('no! no more stuff')
    assert poll.valid_option_idx(0)
    assert poll.valid_option_idx(1)
    assert not poll.valid_option_idx(2)
    assert not poll.valid_option_one_idx(0)
    assert poll.valid_option_one_idx(1)
    assert poll.valid_option_one_idx(2)
    assert poll.current_state == [(['stuff', 'no! no more stuff'], 0)]
    log.debug(poll.current_state)
    poll.user_vote('john', [2, 1])
    log.debug(poll.current_state)
    poll.user_vote('jane', [2])
    log.debug(poll.current_state)
    assert poll.current_state == [(['no! no more stuff'], 4), (['stuff'], 1)]
    poll.user_vote('jack', [1])
    log.debug(poll.current_state)
    poll.user_vote('jill', [1, 2])
    log.debug(poll.current_state)
    assert poll.current_state == [(['stuff', 'no! no more stuff'], 5)]
    poll.user_vote('john', [1, 2])
    log.debug(poll.current_state)
    log.debug(poll.votes)
    assert poll.current_state == [(['stuff'], 6), (['no! no more stuff'], 4)]
