from tempfile import TemporaryDirectory
from pathlib import Path
import json

from ranked_choice_bot.poll import Poll
from ranked_choice_bot.tracker import PollTracker
from ranked_choice_bot.error import NoKnownPollsForConversationId


def test_poll_tracker_basics() -> None:
    tracker = PollTracker()
    assert not tracker.poll_exists('does-not-exist')
    assert not tracker.poll_exists('12345')
    tracker.new_poll('12345', 'Shall we dance?')
    assert tracker.poll_exists('12345')
    assert tracker.get_poll_desc_and_state('12345') == ('Shall we dance?', [])
    assert tracker.get_poll('12345').desc == 'Shall we dance?'
    assert isinstance(tracker.add_option_to_convo_poll('does-not-exist', 'yes'),
                      NoKnownPollsForConversationId)
    assert isinstance(tracker.add_user_vote_to_convo_poll('does-not-exist', 'john', [1, 2, 3]),
                      NoKnownPollsForConversationId)
    assert isinstance(tracker.get_poll_desc_and_state('does-not-exist'),
                      NoKnownPollsForConversationId)
    assert isinstance(tracker.get_poll('does-not-exist'),
                      NoKnownPollsForConversationId)
    assert isinstance(tracker.remove_poll('does-not-exist'),
                      NoKnownPollsForConversationId)
    assert not tracker.add_option_to_convo_poll('12345', 'yes')
    assert not tracker.add_option_to_convo_poll('12345', 'no')
    assert not tracker.add_option_to_convo_poll('12345', 'maybe')
    assert tracker.get_poll_desc_and_state('12345') == ('Shall we dance?', [(['yes', 'no', 'maybe'], 0)])
    assert not tracker.add_user_vote_to_convo_poll('12345', 'john', [1, 2, 3])
    assert not tracker.add_user_vote_to_convo_poll('12345', 'jane', [3, 2, 1])
    assert tracker.get_poll_desc_and_state('12345') == ('Shall we dance?', [(['yes', 'no', 'maybe'], 4)])
    assert not tracker.add_option_to_convo_poll('12345', 'tomorrow')
    assert tracker.get_poll_desc_and_state('12345') == ('Shall we dance?', [(['yes', 'no', 'maybe'], 6), (['tomorrow'], 0)])
    assert not tracker.add_user_vote_to_convo_poll('12345', 'jenn', [4, 3, 2, 1])
    assert tracker.get_poll_desc_and_state('12345') == ('Shall we dance?', [(['maybe'], 9), (['no'], 8), (['yes'], 7), (['tomorrow'], 4)])
    tracker.remove_poll('12345')
    assert not tracker.poll_exists('12345')


def test_poll_tracker_save_load() -> None:
    with TemporaryDirectory() as db_dir:
        database_dir = Path(db_dir)
        tracker = PollTracker().load_db(database_dir)
        assert not tracker.polls
        assert not list(database_dir.iterdir())
        assert not tracker.poll_exists('does-not-exist')
        assert not tracker.poll_exists('12345')
        tracker.new_poll('12345', 'Shall we dance?')
        assert (database_dir / '12345.json').exists()
        serialized = json.loads((database_dir / '12345.json').open().read())
        deserialized = Poll.from_data(serialized)
        assert tracker.polls['12345'].to_data() == serialized
        assert tracker.polls['12345'] == deserialized

        assert not tracker.add_option_to_convo_poll('12345', 'yes')
        assert not tracker.add_option_to_convo_poll('12345', 'no')
        assert not tracker.add_option_to_convo_poll('12345', 'maybe')
        assert tracker.get_poll_desc_and_state('12345') == ('Shall we dance?', [(['yes', 'no', 'maybe'], 0)])
        assert not tracker.add_user_vote_to_convo_poll('12345', 'john', [1, 2, 3])
        assert not tracker.add_user_vote_to_convo_poll('12345', 'jane', [3, 2, 1])
        serialized = json.loads((database_dir / '12345.json').open().read())
        assert deserialized != Poll.from_data(serialized)

        new_tracker = PollTracker().load_db(database_dir)
        assert all(tracker.polls[cid] == new_tracker.polls[cid] for cid in tracker.polls)
        tracker.remove_poll('12345')
        assert not tracker.poll_exists('12345')
        assert not list(database_dir.iterdir())
