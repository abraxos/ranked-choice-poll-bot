# Ranked Choice Bot for Telegram

The ranked choice poll bot is a Telegram chat bot that is capable of running ranked choice polls. Unlike first-past-past-the-post voting, ranked choice has multiple advantages, particularly when it comes to decision-making based on the results of polling in smaller telegram groups. This bot in particular uses a [Borda Count approach](https://en.wikipedia.org/wiki/Borda_count) to ranked-choice voting.

You can access this bot (assuming its up and running) on telegram as `@ranked-choice-poll-bot` and if you want to run one of your own please take a look at the instructions below. To see how to use the bot, simply message `/help` in a private chat with the bot, or add him to a conversation and send `/help`.

This bot is pre-alpha, I am effectively just playing around at this point. Not only do I *not* guarantee that this bot is good for anything, including its advertised purpose, I actually guarantee that at some point it will break and stop functioning properly.

**WARNING** A lot of people do not seem to realize this, so I wanna be clear - Telegram bots have the capability to read *any* and all messages in any chat that they are invited to. This bot is open source to hopefully provide some trust that I am only reading messages that are prefixed with a recognized command, however, this code could be trivially changed to send every single message in any chat to my bot. Please be very cognizant of this. If privacy is a concern (hint: it is), run your own copy of this bot.

### To Do:

This bot is not finished, I intend to add the following features at some point in the future:

1. Anonymous voting (i.e. only the bot should see your vote)
1. A keyboard button prompt for voting instead of having to message the bot as you do now
1. [?] multiple polls per conversation
1. Certain anti-vandalism features such as timeouts for when someone can edit or delete a poll that they did not create

## Installation & Setup

In order to install and run this bot, you will need to get your own Telegram Bot API token. [The process for this is described here](https://core.telegram.org/bots#6-botfather). This repository will not simply work "as is" because it requires credentials that I (hopefully) will not push.

The first step would be to clone this repo. I will be using `/path/to/ranked-choice-poll-bot` to indicate the path to the repository in all examples, but you should replace that with the actual path on your machine. Once you've done that, you can add the credentials as a `creds.yaml` file either in the repo itself `ranked-choice-poll-bot/ranked_choice_bot/` (don't worry, its in the `.gitignore`) or in `~/.config/ranked-choice-bot/creds.yaml` for the user that will be running the bot. The following is an example of a `creds.yaml` file:

```
token: '110201543:AAHdqTcvCH1vGWJxfSeofSAs0K5PALDsaw' # your token should go here, this is the demo token
```

Once you have your credentials set for your bot, you should be able to just install and with pip:

```
pip install /path/to/ranked-choice-poll-bot
ranked-choice-bot
```

## Development Instructions

All of the stuff about credentials apply for developers as well. You should all be using your own API token if you wanna run one of these bots for everyone's security.

Aside from that, for development, you will need a [virtualenv](https://docs.python-guide.org/dev/virtualenvs/) and you will need to install the package with the `-e` parameter inside the virtualenv (which installs the package "in place".

```
(my-ranked-choice-virtualenv) pip install -e /path/to/ranked-choice-poll-bot
```

The code uses pytest for testing, so if you want to run tests, simply execute:

```
pytest -s -vv --cov-report=term-missing --cov-branch --cov=ranked_choice_bot $project_path/tests/unit
```

You can also see the `ranked-choice-bot.sublime-project` file for examples of how I have my sublime text environment and build system set up for this project.