from typing import Dict, List, Optional, Tuple, Union
from pathlib import Path
from json import loads, dumps

from typeguard import typechecked

from ranked_choice_bot.poll import Poll, RankedOptions
from ranked_choice_bot.error import Error, NoKnownPollsForConversationId
from ranked_choice_bot.log import log


class PollTracker():
    @typechecked
    def __init__(self) -> None:
        self.polls: Dict[str, Poll] = {}
        self.database_dir: Optional[Path] = None

    @typechecked
    def load_db(self, database_dir: Path) -> 'PollTracker':
        self.database_dir = database_dir
        for poll_filepath in database_dir.iterdir():
            convo_id = poll_filepath.stem
            poll = Poll.from_data(loads(poll_filepath.open().read()))
            self.polls[convo_id] = poll
        return self

    @typechecked
    def save_poll(self, convo_id: str) -> bool:
        if self.database_dir is not None and convo_id in self.polls:
            poll_filepath = self.database_dir / (convo_id + '.json')
            poll_filepath.open('w').write(dumps(self.polls[convo_id].to_data()))
            return True
        return False

    @typechecked
    def delete_poll(self, convo_id: str) -> bool:
        if self.database_dir is not None and convo_id in self.polls:
            (self.database_dir / (convo_id + '.json')).unlink()
            return True
        return False

    @typechecked
    def poll_exists(self, convo_id: str) -> bool:
        return convo_id in self.polls

    @typechecked
    def get_poll(self, convo_id: str) -> Union[Poll, Error]:
        if not self.poll_exists(convo_id):
            log.warning(f'Cannot retrieve poll - could not find a poll for '
                        f'conversation id: {convo_id}')
            return NoKnownPollsForConversationId(convo_id)
        return self.polls[convo_id]

    @typechecked
    def new_poll(self, convo_id: str, desc: str) -> None:
        log.info(f"Adding a new poll: {desc}")
        poll = Poll(desc)
        self.polls[convo_id] = poll
        self.save_poll(convo_id)

    @typechecked
    def add_option_to_convo_poll(self, convo_id: str,
                                 option: str) -> Optional[Error]:
        if convo_id not in self.polls:
            log.warning(f'Cannot add option "{option}" - could not find a '
                        f'poll for conversation id: {convo_id}')
            return NoKnownPollsForConversationId(convo_id)
        log.info(f'Adding new option "{option}" to poll '
                 f'{convo_id}/"{self.polls[convo_id].desc}"')
        self.polls[convo_id].add_option(option)
        self.save_poll(convo_id)
        return None

    @typechecked
    def add_user_vote_to_convo_poll(self,
                                    convo_id: str,
                                    user_id: str,
                                    ranked: List[int]) -> Optional[Error]:
        if convo_id not in self.polls:
            log.warning(f'Cannot vote in poll - could not find a poll for conversation id: {convo_id}')
            return NoKnownPollsForConversationId(convo_id)
        log.info(f'Submitting vote: {ranked} to {convo_id}/"{self.polls[convo_id].desc}"')
        result = self.polls[convo_id].user_vote(user_id, ranked)
        self.save_poll(convo_id)
        return result

    @typechecked
    def get_poll_desc_and_state(self, convo_id: str) -> Union[Tuple[str, RankedOptions], Error]:
        if convo_id not in self.polls:
            log.warning(f'Cannot fetch poll state - could not find a poll for conversation id: {convo_id}')
            return NoKnownPollsForConversationId(convo_id)
        log.info(f'Fetching poll state for: {convo_id}/"{self.polls[convo_id].desc}"')
        return self.polls[convo_id].desc, self.polls[convo_id].current_state

    @typechecked
    def remove_poll(self, convo_id: str) -> Union[Tuple[str, RankedOptions], Error]:
        if convo_id not in self.polls:
            log.warning(f'Cannot delete poll - could not find a poll for conversation id: {convo_id}')
            return NoKnownPollsForConversationId(convo_id)
        log.info(f'Ending poll: {convo_id}/"{self.polls[convo_id].desc}"')
        desc, state = self.polls[convo_id].desc, self.polls[convo_id].current_state
        self.delete_poll(convo_id)
        del self.polls[convo_id]
        return desc, state
