import os
from pathlib import Path

import yaml

from ranked_choice_bot.log import log


SEARCH_PATHS = [
    Path.home() / '.config' / 'ranked-choice-bot' / 'creds.yaml',
    Path(os.path.realpath(__file__)).parent / 'creds.yaml',
]  # Potential credential configuration paths, in order of preference


for path in SEARCH_PATHS:
    try:
        if path.exists():
            with path.open() as config_file:
                config = yaml.safe_load(config_file.read())
                TOKEN = config['token']
                break
    except Exception as exc:
        print(f'Unable to load credentials from {path}: {str(exc)}')

if TOKEN is not None:
    log.info('Telegram API token loaded successfully')
else:
    log.warning('Unable to load Telegram API token')
