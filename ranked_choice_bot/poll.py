from typing import List, Optional, Tuple, Dict, Any, Union, cast
from itertools import groupby

from typeguard import typechecked

from ranked_choice_bot.error import InvalidOptionsRankingError, OptionNotFound
from ranked_choice_bot.log import log


Strings = List[str]
RankedOption = Tuple[Strings, int]
# Describes the result of the poll which is a list of tuples, each tuple
# containing options with the same score (only one typically unless there is a
# tie) and the score itself
RankedOptions = List[RankedOption]
ScoredOption = Tuple[str, int]
ScoredOptions = List[ScoredOption]


class Vote():
    @staticmethod
    @typechecked
    def from_data(data: Dict[str, Union[str, List[str]]]) -> 'Vote':
        assert 'voter_id' in data, 'Cannot load vote from data, no voter_id'
        assert 'ranked' in data, 'Cannot load vote from data, no voter_id'
        return Vote(data['voter_id'], data['ranked'])

    @typechecked
    def to_data(self) -> Dict[str, Union[str, List[str]]]:
        return {'voter_id': self.voter_id, 'ranked': self.ranked}

    @typechecked
    def __init__(self, voter_id: str, ranked: Strings):
        self.voter_id = voter_id
        self.ranked = ranked

    @typechecked
    def __contains__(self, item: Any) -> bool:
        return item in self.ranked

    @typechecked
    def index(self, item: Any) -> Union[int, OptionNotFound]:
        if item not in self.ranked:
            return cast(OptionNotFound, OptionNotFound(f'{item} not in {self}'))
        return self.ranked.index(item)

    @typechecked
    def __repr__(self) -> str:
        return f'Vote ({self.voter_id}): {self.ranked}'

    @typechecked
    def __eq__(self, other: Any) -> bool:
        if isinstance(other, Vote):
            return bool(self.voter_id == other.voter_id and
                        self.ranked == other.ranked)
        return False


Votes = List[Vote]


def score_in_vote(option: str, options: Strings, vote: Vote) -> int:
    return int(len(options) - vote.index(option)) if option in vote else 0


def score(option: str, options: Strings, votes: Votes) -> int:
    return sum(score_in_vote(option, options, vote) for vote in votes)


def score_group(scored_options: ScoredOptions) -> RankedOption:
    return [o for o, _ in scored_options], scored_options[0][1]


def tied(scored_options: ScoredOptions) -> RankedOptions:
    return [score_group(list(g)) for k, g in groupby(scored_options, key=lambda t: t[1])]


def sort_and_tie(scored_options: ScoredOptions) -> RankedOptions:
    return sorted(tied(scored_options), key=lambda t: t[1], reverse=True)


class Poll():
    @staticmethod
    @typechecked
    def from_data(data: Dict[str,
                             Union[str,
                                   List[str],
                                   dict]]) -> 'Poll':
        assert 'desc' in data, 'Cannot load poll from data, no desc'
        assert 'options' in data, 'Cannot load poll from data, no options'
        assert 'votes' in data, 'Cannot load poll from data, no votes'
        return Poll(desc=data['desc'], options=data['options'],
                    votes={k: Vote.from_data(v) for k, v in data['votes'].items()})

    @typechecked
    def to_data(self) -> Dict[str,
                              Union[str,
                                    List[str],
                                    dict]]:
        return {'desc': self.desc,
                'options': self.options,
                'votes': {k: v.to_data() for k, v in self.votes.items()}}

    @typechecked
    def __init__(self, desc: str,
                 options: Optional[Strings] = None,
                 votes: Optional[Dict[str, Vote]] = None):
        self.desc = desc
        self.options: Strings = options if options is not None else []
        self.votes: Dict[str, Vote] = {} if votes is None else votes

    @typechecked
    def add_option(self, option: str) -> None:
        self.options.append(option)

    @typechecked
    def valid_option_idx(self, idx: int) -> 'bool':
        """Returns true if a given index (starting with 0) is valid"""
        return 0 <= idx < len(self.options)

    @typechecked
    def valid_option_one_idx(self, idx: int) -> bool:
        """Returns true if a given index (starting with 1) is valid"""
        return cast(bool, self.valid_option_idx(idx - 1))

    @typechecked
    def user_vote(self, user_id: str, ranked: List[int]) -> Optional[InvalidOptionsRankingError]:
        """Translates a user ID and a list of option numbers into a vote"""
        log.info(f'Translating {ranked} to {[self.options[idx - 1] for idx in ranked]}')
        return self.vote(Vote(user_id, [self.options[idx - 1] for idx in ranked]))

    @typechecked
    def vote(self, vote: Vote) -> Optional[InvalidOptionsRankingError]:
        if len(vote.ranked) > len(self.options):
            log.error('There are more ranks than there are options')
            return cast(InvalidOptionsRankingError, InvalidOptionsRankingError(f'There are more ranks than there are options: {vote.ranked} vs {self.options}'))
        if not set(vote.ranked) <= set(self.options):
            log.error(f'The ranking contains entries that are not part of the poll: {vote.ranked} vs {self.options}')
            return cast(InvalidOptionsRankingError, InvalidOptionsRankingError(f'The ranking contains entries that are not part of the poll: {vote.ranked} vs {self.options}'))
        self.votes[vote.voter_id] = vote
        return None

    @property
    def scored_options(self) -> ScoredOptions:
        return [(o, score(o, self.options, list(self.votes.values()))) for o in self.options]

    @property
    def current_state(self) -> RankedOptions:
        result: RankedOptions = sort_and_tie(self.scored_options)
        return result

    @typechecked
    def __eq__(self, other: object) -> bool:
        if isinstance(other, Poll):
            if self.desc == other.desc and \
               self.options == other.options and \
               all((uid in other.votes and other.votes[uid] == self.votes[uid]) for uid in self.votes):
                return True
        return False
