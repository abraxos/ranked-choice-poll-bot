from typing import List, Tuple, Optional, Callable, Iterable, Any
from pathlib import Path
from traceback import format_exc
from functools import wraps

from typeguard import typechecked
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram import Bot, Update
import click

from ranked_choice_bot.log import log
from ranked_choice_bot.creds import TOKEN
from ranked_choice_bot.poll import Strings, RankedOptions, Poll
from ranked_choice_bot.tracker import PollTracker


HELP_MSG = """
This is a bot for managing ranked-choice polling. Instead of simply picking one option that you like, you can order the options according to your preference. Results are then ranked in accordance with everyone's preference using a Borda Count approach (https://en.wikipedia.org/wiki/Borda_count).

DISCLAIMER: Please keep in mind that this bot is pre-Alpha, I am mostly just playing around at this point. Not only do I not guarantee that this bot will work, I actually guarantee that it *will* break.

The following commands are available:

/help: Print this message
/newrankedpoll <Question>: Starts a new poll with the given question. You can then add options with the /newoption command. Only one poll is allowed per conversation. You must explicitly end a poll before starting a new one.
/newoption <Option>: Adds an option to the current poll (the bot will assign a numeric ID to this option and send it to you)
/describeranks: Prints out the description of the current poll with options and their IDs
/rankedvote <Option IDs in order of preference>: Submits your vote to the poll. Be sure to include the option IDs in order of preference, separated by spaces like: "1 2 3 5 4"
/ranks: Prints out the description of the poll and the current results
/endrankedpoll: Prints out the final results and closes the current poll (the bot deletes the data)
"""

INVALID_VOTE_FORMAT = "I'm sorry, but '{}' is not a valid ranked-choice vote. \
Please submit your vote as a list of space-separated numbers corresponding to \
options. So for example if a poll has options A, B, and C, and you prefer C \
more than B, and you don't want A at all, then your vote should be: \
/rankedvote 3 2"

POLL_EXISTS = "There is already another poll active in this chat, please \
delete it with /endrankedpoll first."

NO_SUCH_POLL = "There is no poll in this chat. Please create one with /newrankedpoll"


TRACKER_CMD_HANDLERS: List[CommandHandler] = []
TRACKER_W_ARGS_CMD_HANDLERS: List[CommandHandler] = []
TRACKER = PollTracker()


def valid_int(candidate: str) -> bool:
    try:
        int(candidate)
        return True
    except ValueError:
        return False


def unique(candidate: Iterable[Any]) -> bool:
    return len(set(candidate)) == len(candidate)


def state_of_poll(desc_and_state: Tuple[str, RankedOptions]) -> str:
    desc, state = desc_and_state
    total = sum(s * len(o) for o, s in state)
    return f"Poll: {desc}\n - " + \
        '\n - '.join([', '.join(o) + ': {0:.2f}%'.format(s/total * 100 if total != 0 else 0) for o, s in state])


def poll_description(poll: Poll) -> str:
    return f'{poll.desc}\n' + '\n'.join([
        f'{i+1}) {o}' for i, o in enumerate(poll.options)])


TrackedCmdFunc = Callable[[str, str, PollTracker], Optional[str]]
TrackedCmdWrapper = Callable[[Bot, Update], None]
TrackedCmdFuncWrapper = Callable[[TrackedCmdFunc], TrackedCmdFunc]
TrackedCmdFuncWithArgs = Callable[[str, str, PollTracker, Strings], Optional[str]]
TrackedCmdWithArgsWrapper = Callable[[Bot, Update, Strings], None]
TrackedCmdFuncWithArgsWrapper = Callable[[TrackedCmdFuncWithArgs],
                                         TrackedCmdFuncWithArgs]


def tracker_cmd(cmd: str, tracker: PollTracker) -> TrackedCmdFuncWrapper:
    def decorator_tracker_cmd(func: TrackedCmdFunc) -> TrackedCmdWrapper:
        @wraps(func)
        def wrapper_tracker_cmd(bot: Bot,
                                update: Update) -> None:
            try:
                convo_id: str = str(update.message.chat_id)
                username: str = update.effective_user.username

                log.info(f'Received /{cmd} from: {convo_id}/{username}')
                result = func(convo_id, username, tracker)
                if result:
                    bot.send_message(chat_id=update.message.chat_id,
                                     text=result)
            except Exception:  # pylint: disable=W0703
                log.error(f'An unhandled error has occurred on: "/{cmd}"\n{format_exc()}')
        TRACKER_CMD_HANDLERS.append(CommandHandler(cmd, wrapper_tracker_cmd))
        return wrapper_tracker_cmd
    return decorator_tracker_cmd


def tracker_cmd_w_args(cmd: str,
                       tracker: PollTracker) -> TrackedCmdFuncWithArgsWrapper:
    def decorator_tracker_cmd_w_args(func: TrackedCmdFuncWithArgs) -> TrackedCmdWithArgsWrapper:
        @wraps(func)
        def wrapper_tracker_cmd_w_args(bot: Bot,
                                       update: Update,
                                       args: Strings) -> None:
            try:
                convo_id: str = str(update.message.chat_id)
                username: str = update.effective_user.username

                log.info(f'Received /{cmd}: {args} from: {convo_id}/{username}')
                result = func(convo_id, username, tracker, args)
                if result:
                    bot.send_message(chat_id=update.message.chat_id,
                                     text=result)
            except Exception:  # pylint: disable=W0703
                log.error(f'An unhandled error has occurred on: /{cmd}: {args}\n{format_exc()}')
        TRACKER_W_ARGS_CMD_HANDLERS.append(CommandHandler(cmd, wrapper_tracker_cmd_w_args, pass_args=True))
        return wrapper_tracker_cmd_w_args
    return decorator_tracker_cmd_w_args


@typechecked
@tracker_cmd('help', TRACKER)
def help_cmd(_: str, __: str, ___: PollTracker) -> Optional[str]:
    return HELP_MSG


@typechecked
@tracker_cmd('start', TRACKER)
def start_cmd(_: str, __: str, ___: PollTracker) -> Optional[str]:
    return f"Hello! I am the Ranked-choice Poll Bot\n{HELP_MSG}"


@typechecked
@tracker_cmd('describeranks', TRACKER)
def describe_ranks_cmd(convo_id: str, _: str,
                       tracker: PollTracker) -> Optional[str]:
    if not tracker.poll_exists(convo_id):
        log.warning(f"There is no poll for {convo_id}")
        return NO_SUCH_POLL
    return poll_description(tracker.get_poll(convo_id))


@typechecked
@tracker_cmd_w_args('newrankedpoll', TRACKER)
def new_ranked_poll_cmd(convo_id: str, _: str, tracker: PollTracker,
                        args: Strings) -> Optional[str]:
    desc: str = ' '.join(args)
    if tracker.poll_exists(convo_id):
        log.warning(f"There is already a poll in {convo_id}")
        return POLL_EXISTS
    tracker.new_poll(convo_id, desc)
    return f'Created a new ranked-choice poll: "{desc}"'


@typechecked
@tracker_cmd_w_args('newoption', TRACKER)
def new_option_cmd(convo_id: str, _: str, tracker: PollTracker,
                   args: Strings) -> Optional[str]:
    if not tracker.poll_exists(convo_id):
        log.warning(f"There is no poll for {convo_id}")
        return NO_SUCH_POLL
    option: str = ' '.join(args)
    tracker.add_option_to_convo_poll(convo_id, option)
    return f'New poll option:\n{poll_description(tracker.get_poll(convo_id))}'


@typechecked
@tracker_cmd_w_args('rankedvote', TRACKER)
def ranked_vote_cmd(convo_id: str, username: str, tracker: PollTracker,
                    args: Strings) -> Optional[str]:
    if not tracker.poll_exists(convo_id):
        log.warning(f"There is no poll for {convo_id}")
        return NO_SUCH_POLL
    poll = tracker.get_poll(convo_id)
    if not all(valid_int(i) for i in args) or not unique(args) or \
            any(not poll.valid_option_one_idx(int(i)) for i in args):
        log.warning(f"Received invalid vote for {convo_id}/{username}: {args}")
        return INVALID_VOTE_FORMAT.format(' '.join(args))
    error = tracker.add_user_vote_to_convo_poll(convo_id,
                                                username,
                                                [int(i) for i in args])
    if error:
        log.warning("Could not submit vote")
        return INVALID_VOTE_FORMAT.format(' '.join(args))
    log.info("Vote submitted successfully")
    return f'Thanks! {username} I have received your vote'


@typechecked
@tracker_cmd('ranks', TRACKER)
def ranks_cmd(convo_id: str, _: str,
              tracker: PollTracker) -> Optional[str]:
    if not tracker.poll_exists(convo_id):
        log.warning(f"There is no poll for {convo_id}")
        return NO_SUCH_POLL
    return state_of_poll(tracker.get_poll_desc_and_state(convo_id))


@typechecked
@tracker_cmd('endrankedpoll', TRACKER)
def end_ranked_poll_cmd(convo_id: str, _: str,
                        tracker: PollTracker) -> Optional[str]:
    if not tracker.poll_exists(convo_id):
        log.warning(f"There is no poll for {convo_id}")
        return NO_SUCH_POLL
    desc = state_of_poll(tracker.get_poll_desc_and_state(convo_id))
    tracker.remove_poll(convo_id)
    log.info(f"Poll for {convo_id} has been removed")
    return f'Ending: {desc}'


@click.command()
@click.option('-d', '--database', type=click.Path(exists=True,
                                                  file_okay=False,
                                                  dir_okay=True,
                                                  readable=True,
                                                  writable=True,
                                                  resolve_path=True),
              help='A path to a directory that will be used to store poll '
                   'data. If there is already data inside the directory then '
                   'the bot will try to lead state from it. If the data is '
                   'improperly formatted it will crash.')
def run_bot(database: Optional[str]) -> None:
    """
    A very basic robot for ranked choice polling on Telegram. For a
    comprehensive guide on how polling works, launch the bot and send it a
    /help message. The robot runs in-memory by default which means that when
    its restarted all polls and data are lost, however, if you use the
    --database parameter it will save the polls and votes to disk as files
    and can recover them when its restarted.
    """
    if database is not None:
        log.info('Loading from database...')
        TRACKER.load_db(Path(database))

    log.info("Starting ranked choice bot...")
    updater = Updater(token=TOKEN)

    for handler in TRACKER_CMD_HANDLERS + TRACKER_W_ARGS_CMD_HANDLERS:
        updater.dispatcher.add_handler(handler)

    updater.start_polling()


if __name__ == '__main__':
    run_bot()  # pylint: disable=E1120
