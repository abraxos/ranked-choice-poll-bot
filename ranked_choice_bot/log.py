import logging
import logging.config
import os
from pathlib import Path

import yaml


SEARCH_PATHS = [
    Path.home() / '.config' / 'ranked-choice-bot' / 'log.yaml',
    Path(os.path.realpath(__file__)).parent / 'log.yaml',
]  # Potential logging configuration paths, in order of preference


for path in SEARCH_PATHS:
    try:
        if path.exists():
            with path.open() as config_file:
                config = yaml.safe_load(config_file.read())
                logging.config.dictConfig(config)
                break
    except Exception as exc:
        print(f'Could not load configuration from {path}: {str(exc)}')

log = logging.getLogger(__name__)

log.info('Ranked choice bot logging intialized')
