from typeguard import typechecked


class Error():
    @typechecked
    def __init__(self, msg: str):
        self.msg = msg

    @typechecked
    def __repr__(self) -> str:
        return f"{self.__class__.__name__}: {self.msg}"


class InvalidOptionsRankingError(Error):
    pass


class OptionNotFound(Error):
    pass


class NoKnownPollsForConversationId(Error):
    @typechecked
    def __init__(self, convo_id: str) -> None:
        super().__init__(f'No polls exist for conversation: {convo_id}')
