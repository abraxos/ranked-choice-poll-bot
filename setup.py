"""Setup file for a template python client-authenticated webapp."""
from setuptools import setup, find_packages

with open('README.md') as f:
    README = f.read()

with open('requirements.txt') as f:
    REQUIREMENTS = f.readlines()

setup(
    name='passwords',
    version='0.1.0',
    description="A bot for ranked choice polls in Telegram",
    long_description=README,
    author='Eugene Kovalev',
    author_email='eugene+dev@kovalev.io',
    url='https://gitlab.com/abraxos/ranked-choice-bot',
    packages=find_packages(exclude=('tests', 'docs')),
    install_requires=REQUIREMENTS,
    entry_points={'console_scripts': ['ranked-choice-bot='
                                      'ranked_choice_bot.bot:run_bot']},
)
